<!--
SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
SPDX-License-Identifier: BSD-2-Clause
-->

# Download

These are the released versions of [timelimit](index.md) available for download.

## [1.9.2] - 2021-05-16

### Source tarball

- [timelimit-1.9.2.tar.gz](https://devel.ringlet.net/files/sys/timelimit/timelimit-1.9.2.tar.gz)
  (with [a PGP signature](https://devel.ringlet.net/files/sys/timelimit/timelimit-1.9.2.tar.gz.asc))
- [timelimit-1.9.2.tar.bz2](https://devel.ringlet.net/files/sys/timelimit/timelimit-1.9.2.tar.bz2)
  (with [a PGP signature](https://devel.ringlet.net/files/sys/timelimit/timelimit-1.9.2.tar.bz2.asc))
- [timelimit-1.9.2.tar.xz](https://devel.ringlet.net/files/sys/timelimit/timelimit-1.9.2.tar.xz)
  (with [a PGP signature](https://devel.ringlet.net/files/sys/timelimit/timelimit-1.9.2.tar.xz.asc))

## [1.9.1] - withdrawn

## [1.9.0] - 2018-04-14

### Source tarball

- [timelimit-1.9.0.tar.gz](https://devel.ringlet.net/files/sys/timelimit/timelimit-1.9.0.tar.gz)
  (with [a PGP signature](https://devel.ringlet.net/files/sys/timelimit/timelimit-1.9.0.tar.gz.asc))
- [timelimit-1.9.0.tar.bz2](https://devel.ringlet.net/files/sys/timelimit/timelimit-1.9.0.tar.bz2)
  (with [a PGP signature](https://devel.ringlet.net/files/sys/timelimit/timelimit-1.9.0.tar.bz2.asc))
- [timelimit-1.9.0.tar.xz](https://devel.ringlet.net/files/sys/timelimit/timelimit-1.9.0.tar.xz)
  (with [a PGP signature](https://devel.ringlet.net/files/sys/timelimit/timelimit-1.9.0.tar.xz.asc))

## [1.8.2] - 2017-11-20

### Source tarball

- [timelimit-1.8.2.tar.gz](https://devel.ringlet.net/files/sys/timelimit/timelimit-1.8.2.tar.gz)
  (with [a PGP signature](https://devel.ringlet.net/files/sys/timelimit/timelimit-1.8.2.tar.gz.asc))
- [timelimit-1.8.2.tar.bz2](https://devel.ringlet.net/files/sys/timelimit/timelimit-1.8.2.tar.bz2)
  (with [a PGP signature](https://devel.ringlet.net/files/sys/timelimit/timelimit-1.8.2.tar.bz2.asc))
- [timelimit-1.8.2.tar.xz](https://devel.ringlet.net/files/sys/timelimit/timelimit-1.8.2.tar.xz)
  (with [a PGP signature](https://devel.ringlet.net/files/sys/timelimit/timelimit-1.8.2.tar.xz.asc))

## [1.8.1] - 2016-04-15

### Source tarball

- [timelimit-1.8.1.tar.gz](https://devel.ringlet.net/files/sys/timelimit/timelimit-1.8.1.tar.gz)
  (with [a PGP signature](https://devel.ringlet.net/files/sys/timelimit/timelimit-1.8.1.tar.gz.asc))
- [timelimit-1.8.1.tar.bz2](https://devel.ringlet.net/files/sys/timelimit/timelimit-1.8.1.tar.bz2)
  (with [a PGP signature](https://devel.ringlet.net/files/sys/timelimit/timelimit-1.8.1.tar.bz2.asc))
- [timelimit-1.8.1.tar.xz](https://devel.ringlet.net/files/sys/timelimit/timelimit-1.8.1.tar.xz)
  (with [a PGP signature](https://devel.ringlet.net/files/sys/timelimit/timelimit-1.8.1.tar.xz.asc))

## [1.8] - 2011-04-13

### Source tarball

- [timelimit-1.8.tar.gz](https://devel.ringlet.net/files/sys/timelimit/timelimit-1.8.tar.gz)
  (with [a PGP signature](https://devel.ringlet.net/files/sys/timelimit/timelimit-1.8.tar.gz.asc))

## [1.7] - 2010-09-30

### Source tarball

- [timelimit-1.7.tar.gz](https://devel.ringlet.net/files/sys/timelimit/timelimit-1.7.tar.gz)
  (with [a PGP signature](https://devel.ringlet.net/files/sys/timelimit/timelimit-1.7.tar.gz.asc))
- [timelimit-1.7.tar.bz2](https://devel.ringlet.net/files/sys/timelimit/timelimit-1.7.tar.bz2)
  (with [a PGP signature](https://devel.ringlet.net/files/sys/timelimit/timelimit-1.7.tar.bz2.asc))

## [1.6] - 2010-06-26

### Source tarball

- [timelimit-1.6.tar.gz](https://devel.ringlet.net/files/sys/timelimit/timelimit-1.6.tar.gz)
  (with [a PGP signature](https://devel.ringlet.net/files/sys/timelimit/timelimit-1.6.tar.gz.asc))

## [1.5] - 2009-10-30

### Source tarball

- [timelimit-1.5.tar.gz](https://devel.ringlet.net/files/sys/timelimit/timelimit-1.5.tar.gz)
  (with [a PGP signature](https://devel.ringlet.net/files/sys/timelimit/timelimit-1.5.tar.gz.asc))

## [1.4] - 2008-11-12

### Source tarball

- [timelimit-1.4.tar.gz](https://devel.ringlet.net/files/sys/timelimit/timelimit-1.4.tar.gz)
  (with [a PGP signature](https://devel.ringlet.net/files/sys/timelimit/timelimit-1.4.tar.gz.asc))

## [1.3] - 2008-09-03

### Source tarball

- [timelimit-1.3.tar.gz](https://devel.ringlet.net/files/sys/timelimit/timelimit-1.3.tar.gz)
  (with [a PGP signature](https://devel.ringlet.net/files/sys/timelimit/timelimit-1.3.tar.gz.asc))

## [1.2] - 2008-08-22

### Source tarball

- [timelimit-1.2.tar.gz](https://devel.ringlet.net/files/sys/timelimit/timelimit-1.2.tar.gz)
  (with [a PGP signature](https://devel.ringlet.net/files/sys/timelimit/timelimit-1.2.tar.gz.asc))

## [1.1] - 2007-12-07

### Source tarball

- [timelimit-1.1.tar.gz](https://devel.ringlet.net/files/sys/timelimit/timelimit-1.1.tar.gz)
  (with [a PGP signature](https://devel.ringlet.net/files/sys/timelimit/timelimit-1.1.tar.gz.asc))

## [1.0] - 2007-11-28

### Source tarball

- [timelimit-1.0.tar.gz](https://devel.ringlet.net/files/sys/timelimit/timelimit-1.0.tar.gz)
  (with [a PGP signature](https://devel.ringlet.net/files/sys/timelimit/timelimit-1.0.tar.gz.asc))

[1.9.2]: https://gitlab.com/timelimit/timelimit/-/tags/release%2F1.9.2
[1.9.1]: https://gitlab.com/timelimit/timelimit/-/tags/release%2F1.9.1
[1.9.0]: https://gitlab.com/timelimit/timelimit/-/tags/release%2F1.9.0
[1.8.2]: https://gitlab.com/timelimit/timelimit/-/tags/release%2F1.8.2
[1.8.1]: https://gitlab.com/timelimit/timelimit/-/tags/release%2F1.8.1
[1.8]: https://gitlab.com/timelimit/timelimit/-/tags/release%2F1.8
[1.7]: https://gitlab.com/timelimit/timelimit/-/tags/release%2F1.7
[1.6]: https://gitlab.com/timelimit/timelimit/-/tags/release%2F1.6
[1.5]: https://gitlab.com/timelimit/timelimit/-/tags/release%2F1.5
[1.4]: https://gitlab.com/timelimit/timelimit/-/tags/release%2F1.4
[1.3]: https://gitlab.com/timelimit/timelimit/-/tags/release%2F1.3
[1.2]: https://gitlab.com/timelimit/timelimit/-/tags/release%2F1.2
[1.1]: https://gitlab.com/timelimit/timelimit/-/tags/release%2F1.1
[1.0]: https://gitlab.com/timelimit/timelimit/-/tags/release%2F1.0
