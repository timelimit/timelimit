<!--
SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
SPDX-License-Identifier: BSD-2-Clause
-->

# Changelog

All notable changes to the timelimit project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.9.2] - 2021-05-16

### Fixes

- Happy 20th birthday, timelimit!
- Fix a bug with propagating a SIGKILL or SIGSTOP signal on
  at least some Linux/glibc systems

## 1.9.1 - (withdrawn)

## [1.9.0] - 2018-04-14

### Fixes

- Drop an unused preprocessor conditional
- Mark some function parameters as used when some platform
  features are not present
- Define `_POSIX_C_SOURCE` and `_XOPEN_SOURCE` by default

### Additions

- Add the `--features` command-line option that always lists
  the "timelimit" feature (with the program version) and also
  lists the "subsecond" feature (version 1.0) if available
- Autodetect platform features instead of specifying them in
  the Makefile
- Use the FSF TAP shell library
- Add the build-and-test-all tool to, well, test builds with
  and without the options supported on the current platform

### Other changes

- Slightly change the format of the usage error message

## [1.8.2] - 2017-11-20

### Fixes

- Allow the `install(1)` invocations to be overridden

## [1.8.1] - 2016-04-15

### Additions

- Install the tests as documentation examples

### Other changes

- Remove the `$Ringlet` tags from the source files
- Use the C99 bool type
- Use the C99 const keyword and in-place variable declarations

## [1.8] - 2011-04-13

### Fixes

- `getopt()` returns -1 on error, not `EOF`

### Additions

- Add a simple test suite compatible with Perl's TAP framework
- Add the ability to specify signals by name and the `-l`
  command-line option to list the available signal names.
  Debian: #621775

## [1.7] - 2010-09-30

### Fixes

- Report the actual error condition if the waitpid() call fails
- Do not allow SIGCHLD to interrupt waitpid() after the kill

### Additions

- Implement microsecond wait time precision using `setitimer(2)`
  Debian: #597824

### Other changes

- Remove the `warn()` function that is never used even on platforms
  that lack the `err(3)` family of functions
- Remove the `cvs_id` string

## [1.6] - 2010-06-26

### Fixes

- Rename the `ChangeLog` to `NEWS`, since that's what it is
- Honor `CPPFLAGS`

## [1.5] - 2009-10-30

### Additions

- Add the `__dead2` definition for no-return functions
- Add the `-p` flag to propagate information about the child
  process being terminated by a signal

## [1.4] - 2008-11-12

### Fixes

- Correct options parsing on GNU systems - do not let timelimit
  grab the option arguments passed to the executed program
  Debian: #505140, reported by John Hasler <jhasler@debian.org>

## [1.3] - 2008-09-03

### Fixes

- Fix the `$Ringlet` VCS keyword in the source files

## [1.2] - 2008-08-22

### Fixes

- Define `_GNU_SOURCE` to properly use the Linux header files

### Other changes

- Return the child process exit code instead of always exiting
  with 0

## [1.1] - 2007-12-07

### Fixes

- Move the manual page from section 8 to section 1

## [1.0] - 2007-11-28

### Fixes

- Attempt to create the installation directories
- Style fixes to the manual page

### Additions

- Add an `install` target to the Makefile
- Build and install the gzipped manual page, too
- Add a two-clause BSD copyright to all files

### Other changes

- Remove the `TARGETS` file that was never used as intended
- Break the web-visible documentation out of the source tree

## [1.0pre3] - 2001-06-09

### Fixes

- Actually add the `-q` flag to the `getopt()` arguments (ARGH!)
- Use `envopts[]` for time/sig variable setting (both env and cmdline)
- Warn when no time/sig arguments specified
- Silence a few gcc warnings
- Removed `CC`, `CFLAGS` initial settings from Makefile

## 1.0pre2 - 2001-05-21

### Additions

- Added the `-q` flag to not output messages about signals sent
  to the child process
- Added `timelimit.8` manpage [roam/alpha]
- Added killtime/warntime/killsig/warnsig defaults

### Other changes

- Changed the `execv()` call to `execvp()`, so `PATH` is searched for
  the specified command, as suggested by alpha
- Changed the default compiler to gcc, as suggested by alpha

## 1.0pre1 - 2001-05-16

### Started

- First rolled-together prerelease.

[Unreleased]: https://gitlab.com/timelimit/timelimit/-/compare/release%2F1.9.2...master
[1.9.2]: https://gitlab.com/timelimit/timelimit/-/compare/release%2F1.9.0...release%2F1.9.2
[1.9.0]: https://gitlab.com/timelimit/timelimit/-/compare/release%2F1.8.2...release%2F1.9.0
[1.8.2]: https://gitlab.com/timelimit/timelimit/-/compare/release%2F1.8.1...release%2F1.8.2
[1.8.1]: https://gitlab.com/timelimit/timelimit/-/compare/release%2F1.8...release%2F1.8.1
[1.8]: https://gitlab.com/timelimit/timelimit/-/compare/release%2F1.7...release%2F1.8
[1.7]: https://gitlab.com/timelimit/timelimit/-/compare/release%2F1.6...release%2F1.7
[1.6]: https://gitlab.com/timelimit/timelimit/-/compare/release%2F1.5...release%2F1.6
[1.5]: https://gitlab.com/timelimit/timelimit/-/compare/release%2F1.4...release%2F1.5
[1.4]: https://gitlab.com/timelimit/timelimit/-/compare/release%2F1.3...release%2F1.4
[1.3]: https://gitlab.com/timelimit/timelimit/-/compare/release%2F1.2...release%2F1.3
[1.2]: https://gitlab.com/timelimit/timelimit/-/compare/release%2F1.1...release%2F1.2
[1.1]: https://gitlab.com/timelimit/timelimit/-/compare/release%2F1.0...release%2F1.1
[1.0]: https://gitlab.com/timelimit/timelimit/-/compare/release%2F1.0pre3...release%2F1.0
[1.0pre3]: https://gitlab.com/timelimit/timelimit/-/tags/release%2F1.0pre3
