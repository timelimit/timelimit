<!--
SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
SPDX-License-Identifier: BSD-2-Clause
-->

# timelimit - limit a process's absolute execution time

\[[Home][ringlet-home] | [Download](download.md) | [GitLab][gitlab] | [ReadTheDocs][readthedocs]\]

## Overview

The `timelimit` tool executes a command and terminates the spawned process after
a given time with a given signal.
A "warning" signal is sent first, then, after a timeout, a "kill" signal,
similar to the way `init(8)` operates on shutdown.

## Examples

The following examples are shown as given to the shell:

``` sh
timelimit -p /usr/local/bin/rsync rsync://some.host/dir /opt/mirror
```

Run the `rsync` program to mirror a WWW or FTP site and kill it if it
runs longer than 1 hour (that is 3600 seconds) with `SIGTERM`.
If the rsync process does not exit after receiving the `SIGTERM`,
`timelimit` issues a `SIGKILL` 120 seconds after the `SIGTERM`.
If the `rsync` process is terminated by a signal, `timelimit` will itself
raise this signal.

``` sh
tcpserver 0 8888 timelimit -t600 -T300 /opt/services/chat/stats
```

Start a `tcpserver` process listening on tcp port 8888; each client connection shall
invoke an instance of an IRC statistics tool under `/opt/services/chat` and
kill it after 600 seconds have elapsed.
If the `stats` process is still running after the `SIGTERM`, it will be killed by
a `SIGKILL` sent 300 seconds later.

``` sh
env WARNTIME=4.99 WARNSIG=1 KILLTIME=1.000001 timelimit sh stats.sh
```

Start a shell script and kill it with a `SIGHUP` in a little under 5 seconds.
If the shell gets stuck and does not respond to the `SIGHUP`, kill it
with the default `SIGKILL` just a bit over a second afterwards.

## Contact

The `timelimit` utility was written by [Peter Pentchev][roam].
It is developed in [a GitLab repository][gitlab]. This documentation is
hosted at [Ringlet][ringlet-home] with a copy at [ReadTheDocs][readthedocs].

[roam]: mailto:roam@ringlet.net "Peter Pentchev"
[gitlab]: https://gitlab.com/timelimit/timelimit "The timelimit GitLab repository"
[readthedocs]: https://timelimit.readthedocs.io/ "The timelimit ReadTheDocs page"
[ringlet-home]: https://devel.ringlet.net/sysutils/timelimit/ "The Ringlet timelimit homepage"
