#!/bin/sh
#
# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause

if [ -f 'tap-functions.sh' ]; then
	. tap-functions.sh
elif [ -f 't/tap-functions.sh' ]; then
	. t/tap-functions.sh
else
	echo 'Bail out! Could not find tap-functions.sh'
	exit 99
fi

[ -z "$TIMELIMIT" ] && TIMELIMIT='./timelimit'

plan_ 9

diag_ 'kill a shell script during the sleep, only execute a single echo'
v=`$TIMELIMIT -t 1 sh -c 'echo 1; sleep 3; echo 2' 2>&1 > /dev/null`
res="$?"
if [ "$res" = 143 ]; then ok_; else not_ok_ "exit code $res"; fi
if expr "x$v" : 'x.*warning' > /dev/null; then ok_; else not_ok_ "v is '$v'"; fi
if ! expr "x$v" : 'x.*kill' > /dev/null; then ok_; else not_ok_ "v is '$v'"; fi

diag_ 'catch the warning signal, run two out of three echos'
v=`$TIMELIMIT -t 1 -T 4 sh -c 'trap "" TERM; echo 1; sleep 3; echo 2; sleep 3; echo 3' 2>&1 > /dev/null`
res="$?"
if [ "$res" = 137 ]; then ok_; else not_ok_ "exit code $res"; fi
if expr "x$v" : 'x.*warning' > /dev/null; then ok_; else not_ok_ "v is '$v'"; fi
if expr "x$v" : 'x.*kill' > /dev/null; then ok_; else not_ok_ "v is '$v'"; fi

diag_ 'now do the same, but be vewwy, vewwy quiet...'
v=`$TIMELIMIT -q -t 1 -T 4 sh -c 'trap "" TERM; echo 1; sleep 3; echo 2; sleep 3; echo 3' 2>&1 > /dev/null`
res="$?"
if [ "$res" = 137 ]; then ok_; else not_ok_ "exit code $res"; fi
if ! expr "x$v" : 'x.*warning' > /dev/null; then ok_; else not_ok_ "v is '$v'"; fi
if ! expr "x$v" : 'x.*kill' > /dev/null; then ok_; else not_ok_ "v is '$v'"; fi
