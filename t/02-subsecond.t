#!/bin/sh
#
# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause

if [ -f 'tap-functions.sh' ]; then
	. tap-functions.sh
elif [ -f 't/tap-functions.sh' ]; then
	. t/tap-functions.sh
else
	echo 'Bail out! Could not find tap-functions.sh'
	exit 99
fi

[ -z "$TIMELIMIT" ] && TIMELIMIT='./timelimit'

if ! $TIMELIMIT --features | fgrep -qe ' subsecond='; then
	skip_all_ 'Subsecond precision not supported, skipping this test set'
fi

plan_ 7

diag_ 'kill a shell script during the sleep, only execute a single echo'
v=`$TIMELIMIT -t 0.7 sh -c 'echo 1; sleep 1; echo 2' 2>/dev/null`
res="$?"
if [ "$res" = 143 ]; then ok_; else not_ok_ "exit code $res"; fi
if expr "x$v" : 'x1' > /dev/null; then ok_; else not_ok_ "v is '$v'"; fi
if ! expr "x$v" : 'x.*2' > /dev/null; then ok_; else not_ok_ "v is '$v'"; fi

diag_ 'catch a signal, kill with a different one'
v=`$TIMELIMIT -t 0.7 -s 1 -T 0.8 -S 15 sh -c 'trap "" HUP; echo 1; sleep 1; echo 2; sleep 1; echo 3' 2>/dev/null`
res="$?"
if [ "$res" = 143 ]; then ok_; else not_ok_ "exit code $res"; fi
if expr "x$v" : 'x1' > /dev/null; then ok_; else not_ok_ "v is '$v'"; fi
if expr "x$v" : 'x.*2' > /dev/null; then ok_; else not_ok_ "v is '$v'"; fi
if ! expr "x$v" : 'x.*3' > /dev/null; then ok_; else not_ok_ "v is '$v'"; fi
