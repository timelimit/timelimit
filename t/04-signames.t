#!/bin/sh
#
# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause

if [ -f 'tap-functions.sh' ]; then
	. tap-functions.sh
elif [ -f 't/tap-functions.sh' ]; then
	. t/tap-functions.sh
else
	echo 'Bail out! Could not find tap-functions.sh'
	exit 99
fi

[ -z "$TIMELIMIT" ] && TIMELIMIT='./timelimit'

plan_ 10

diag_ 'list the available signal names'
v=`$TIMELIMIT -l 2>/dev/null`
res="$?"
if [ "$res" = 0 ]; then ok_; else not_ok_ "exit code $res"; fi
if expr "x$v" : 'x.*TERM'; then ok_; else not_ok_ "no TERM in timelimit -l"; fi
if expr "x$v" : 'x.*HUP'; then ok_; else not_ok_ "no HUP in timelimit -l"; fi
if ! expr "x$v" : 'x.*HUX'; then ok_; else not_ok_ "found HUX in timelimit -l"; fi
if ! expr "x$v" : 'x.*TERX'; then ok_; else not_ok_ "found TERX in timelimit -l"; fi

diag_ 'use signal names'
v=`$TIMELIMIT -t 1 -s HUP -T 1 -S TERM sh -c 'trap "" HUP; echo 1; sleep 3; echo 2' 2>/dev/null`
res="$?"
if [ "$res" = 143 ]; then ok_; else not_ok_ "exit code $res"; fi
if expr "x$v" : 'x1' > /dev/null; then ok_; else not_ok_ "v is '$v'"; fi
if ! expr "x$v" : 'x.*2' > /dev/null; then ok_; else not_ok_ "v is '$v'"; fi

diag_ 'use an invalid signal name for the warning signal'
v=`$TIMELIMIT -t 1 -s HUX -T 1 -S TERM sh -c 'trap "" HUP; echo 1; sleep 3; echo 2' 2>/dev/null`
res="$?"
if [ "$res" = 64 ]; then ok_; else not_ok_ "exit code $res"; fi

diag_ 'use an invalid signal name for the kill signal'
v=`$TIMELIMIT -t 1 -s HUP -T 1 -S TERX sh -c 'trap "" HUP; echo 1; sleep 3; echo 2' 2>/dev/null`
res="$?"
if [ "$res" = 64 ]; then ok_; else not_ok_ "exit code $res"; fi
