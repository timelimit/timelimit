/**
 * SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
 * SPDX-License-Identifier: BSD-2-Clause
 */

#include <sys/time.h>
#include <stdlib.h>

int main(void) {
	return setitimer(ITIMER_REAL, NULL, NULL);
}
