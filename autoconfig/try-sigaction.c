/**
 * SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
 * SPDX-License-Identifier: BSD-2-Clause
 */

#include <signal.h>
#include <stdlib.h>

int main(void) {
	return sigaction(SIGTERM, NULL, NULL);
}
