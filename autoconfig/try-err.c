/**
 * SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
 * SPDX-License-Identifier: BSD-2-Clause
 */

#include <err.h>

int main(void) {
	err(1, "Bad, bad, bad");
	/* NOTREACHED */
}
